# ssh

Ansible role that installs and configures OpenSSH server.

**NOTE:** If the host will use a **custom SSH port number**,
you need to configure that yourself, manually. Not something
this role handles at present.


## Links and notes

### Preset SSH hostkeys for LXD containers?

During testing I spin up and delete the same container many times.
Having to clear the corresponding line from `~/.ssh/known_hosts` every
time can get annoying.

A simple solution would be to add `StrictHostKeyChecking no` to my ssh
config snippet for that host, but another approach would be to preset
the container's SSH server hostkeys in the playbook (thus having the
same hostkeys every time the container is re-provisioned).
I'm not sure which approach would be considered less safe.

+ https://serverfault.com/questions/910071/how-to-generate-host-ssh-keys-via-ansible3
+ https://askubuntu.com/questions/76337/where-is-the-ssh-server-fingerprint-generated-stored
+ https://www.ssh.com/academy/ssh/host-key
